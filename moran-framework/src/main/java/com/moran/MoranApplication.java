package com.moran;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author qiko
 */
@SpringBootApplication
public class MoranApplication {

    public static void main(String[] args) {
        SpringApplication.run(MoranApplication.class, args);
    }
}
